//
//  Profile+CoreDataProperties.swift
//  TutorialDatabase
//
//  Created by ali on 26/01/19.
//  Copyright © 2019 nusanet. All rights reserved.
//
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var name: String?
    @NSManaged public var age: String?
    @NSManaged public var address: String?
    @NSManaged public var hp: String?

}
