//
//  ViewController.swift
//  TutorialDatabase
//
//  Created by ali on 25/01/19.
//  Copyright © 2019 nusanet. All rights reserved.
//

import UIKit

class InputDataController: UIViewController {
    
    @IBOutlet weak var fieldName: UITextField!
    @IBOutlet weak var fieldAge: UITextField!
    @IBOutlet weak var fieldAddress: UITextField!
    @IBOutlet weak var fieldHp: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func saveDataProfile(_ sender: UIButton) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DatabaseViewController {
            destination.name = fieldName.text
            destination.age = fieldAge.text
            destination.address = fieldAddress.text
            destination.hp = fieldHp.text
        }
    }
}

